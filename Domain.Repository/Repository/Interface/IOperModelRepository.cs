﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IOperModelRepository
    {
        bool SaveOperModel(Domain.Entities.OperModel entity);

        IList<Domain.Entities.OperModel> GetOperModels();

        bool UpdateOperModel(Domain.Entities.OperModel entity);

        bool DeleteOperModels(IList<Domain.Entities.OperModel> entities);
    }
}