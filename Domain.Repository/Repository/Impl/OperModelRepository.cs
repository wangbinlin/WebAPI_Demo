﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommHelper;

namespace Domain.Repository
{
    public class OperModelRepository : Repository.RepositoryBase<Domain.Entities.OperModel>, IOperModelRepository
    {
        private Persistence.IDao.IOperModelDao _OperModelDao { get; set; }

        public bool SaveOperModel(Domain.Entities.OperModel entity)
        {
            var target = new Persistence.Entities.OperModel();
            entity.CopyTo(target);
            return _OperModelDao.SaveOperModel(target);
        }

        public IList<Domain.Entities.OperModel> GetOperModels()
        {
            List<Domain.Entities.OperModel> items = new List<Entities.OperModel>();
            var list = _OperModelDao.GetOperModels();
            foreach (var entity in list)
            {
                var item = new Entities.OperModel();
                entity.CopyTo(item);
                items.Add(item);
            }
            return items;
        }

        public bool UpdateOperModel(Domain.Entities.OperModel entity)
        {
            var target = new Persistence.Entities.OperModel();
            entity.CopyTo(target);
            return _OperModelDao.UpdateOperModel(target);
        }

        public bool DeleteOperModels(IList<Domain.Entities.OperModel> entities)
        {
            List<Persistence.Entities.OperModel> list = new List<Persistence.Entities.OperModel>();
            foreach (var entity in entities)
            {
                var item = new Persistence.Entities.OperModel();
                entity.CopyTo(item);
                list.Add(item);
            }
            return _OperModelDao.DeleteOperModels(list);
        }
    }
}