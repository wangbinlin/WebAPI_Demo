﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Entities
{
    public partial class OperModel : TransactionBaseEntity
    {
        public OperModel()
        {
            OnCreated();
        }

        partial void OnCreated();

        public virtual string Id
        {
            get;
            set;
        }

        public virtual System.Nullable<decimal> x
        {
            get;
            set;
        }

        public virtual System.Nullable<decimal> y
        {
            get;
            set;
        }

        public virtual System.Nullable<decimal> result
        {
            get;
            set;
        }

        public virtual string Operation
        {
            get;
            set;
        }
    }
}