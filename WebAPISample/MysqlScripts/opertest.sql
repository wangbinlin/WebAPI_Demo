﻿/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : opertest

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2017-11-23 10:43:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for oper_model
-- ----------------------------
DROP TABLE IF EXISTS `oper_model`;
CREATE TABLE `oper_model` (
  `id` char(32) NOT NULL COMMENT '主键，GUID',
  `x` decimal(10,2) DEFAULT NULL,
  `y` decimal(10,2) DEFAULT NULL,
  `result` decimal(10,2) DEFAULT NULL,
  `operation` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oper_model
-- ----------------------------
INSERT INTO `oper_model` VALUES ('5f5377d91e2f4725a3aca1dbf53fda73', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('eabd0309386547a69c50b0e2399d34f0', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('6a5c78563ee64baabbb80559223cfbb7', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('8fb68fffd5f24942a46f6706a44e5aa9', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('9e87669b79df4fd08a86fe11e1af2d3e', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('bdd2f6ccfd2844c28976679fe4b4fa14', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('d1fc6cbbb60a43aca1cfcf4e5d2486ca', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('7849205bffc844938d5c7e9b918f007a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('a07cace00ff04e58b6f0038f0779fac7', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('19c0fe477d3f4d859a3b6efb9179ee13', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('8d4504bfa6a847efbd125be9bb9675dc', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('eaafef46a7c24d01b67c5aae8d3b21e8', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('5831936904264f8e988b5bb739fec2b3', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('36d09bed3b9748be961c29cff0ccf77a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('e57b8c7016bc471c9ae88ffdaa937fa8', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('89ef3b7751a5444991effb32fa314b65', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('9e376b7b340049f89e0bb077af6ec0ee', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('1c6251602ff54fd1a67e9db199a86afc', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('b0a38052474c48798d14163dd8caf81c', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('72749b49310a40ae996c386a06dd3cea', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('64888b4775794fdc8063ea96ee6243e9', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('e14db722e2824871b90349ed29e3939f', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('cd4234a7118743148225daf47c144520', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('e942b7f10c314c759fa2fd7ff237ee98', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('d175a11f10c240df875dcd4d8f77edae', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('08d4020e243f422bb77df40c21d09e47', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('51b900e319e34035a88d0a030de8be79', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('d193d4239fc94c3f84b41fb841041ed6', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('7e3580d5b20d4fa68fcd62ad07d5d04b', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('fa1ea6216870419ebd0b3355fcb0d898', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('c60c755ed4154fb6a571c3aad0750cb2', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('d75e55bb51054618825cbdce7aa8b4df', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('32c1f3518a1f4ea9947889fd1d227ebc', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('b03d33a38a044080999ae764306a2fb5', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('987effdc17a644eda2f326aa514cc2a2', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('fb8e94c73c5941a79fee518d3680020f', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('097930745a04479096e39f4b2bfee605', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('6fdd8256b7be4f798ed4b144fddcaa63', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('33d33b64ed124b0ba5d36d73de7f8309', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('6db7a73dd0b94fcf8b69d6170d8be03a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('e443b055782d4097acb1043bf1980231', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('5869c547f61b4834a4555e12eb4d341a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('fba4648bf89c4f1fbe24a482c0024bfc', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('a75a8d28dee940b4be6bad0f65a23acb', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('5f45044130f44e3eb8ad9e046ed9ff6a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('e5b8d6903c6f44bab3aad3e4975a8006', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('96b44ed619f94b90814f351d5fe94411', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('0db3cde0819f47778b3d046d7d21a248', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('73a3c1d305404c32924fd0ec5acda309', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('3ae4296dc8d543bca360bc7db9fb1fea', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('5627e294c61742bcb23a2d6fe4fa706e', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('95456728bdf346de920cd44ff7f89be4', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('fac1e4c293a741adb5b809bc7673e029', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('1d62b4f7c7da426796ccc0369d0b4068', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('7da773f7dcb64408818254271ff662fb', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('d3c75cc5c42642f09e4b872416c8c114', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('b79204a7a6b244a0beda24800cd8f88b', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('2db951c50c0144c59f008bc1f67a9e45', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('2228c197066f45c18c90f08081646266', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('06e7b2d7a8c94c77ae234c64f03fdc2a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('5d353b0b3fc34fa89d058c7185647769', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('2e543aa2d5514e15bbd20320cc057b50', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('edd6e7fcac754bfea46c03d46f75006a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('e94b7323aaf64c069132402bbbf30a2a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('9aeb1c5637374d4fa29d8e6bf3dd2a2e', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('2f40cdf13eea495bb5519062d37468da', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('0fbbb44696a147d69def9bdac0d61853', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('b06e458b07244557948ee560015b4e7a', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('34dd3f3312a94be2ac5e9b44ada5a38e', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('dea485f3153f4b628725d49bb34db558', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('c1798fcbd09a419dab7efb57dded5b72', '1.00', '2.00', '3.00', 'Add');
INSERT INTO `oper_model` VALUES ('73685540d0894e36a4e608b126643272', '1.00', '2.00', '3.00', 'UpdateModel');
INSERT INTO `oper_model` VALUES ('464d8dda689a4ed0a283afe990c11bae', '1.00', '2.00', '3.00', 'UpdateModel');
INSERT INTO `oper_model` VALUES ('bfe6b9d898cb45bea4c872bfbbbdb288', '1.00', '2.00', '3.00', 'UpdateModel');
INSERT INTO `oper_model` VALUES ('4ce2c60527a54186ae460176237f4e34', '1.00', '2.00', '3.00', 'UpdateModel');
INSERT INTO `oper_model` VALUES ('0ce66c56d81a4553b20f6341c5abd142', '1.00', '2.00', '3.00', 'UpdateModel');
INSERT INTO `oper_model` VALUES ('3c51008be81043aea5ea04bef7a33ab2', '1.00', '2.00', '3.00', 'UpdateModel');
INSERT INTO `oper_model` VALUES ('927aeecf7de14e51bc30b806da762f88', '1.00', '2.00', '3.00', 'UpdateModel');
INSERT INTO `oper_model` VALUES ('b23768dd2bad46969feb17bd72008509', '1.00', '2.00', '3.00', 'UpdateModel');
SET FOREIGN_KEY_CHECKS=1;
