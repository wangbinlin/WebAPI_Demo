﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebAPISample.Controllers;

namespace WebAPISample.Tests.Controllers
{
    [TestClass]
    public class TestOperationControllerTest
    {
        [TestMethod]
        public void Test()
        {
            // Arrange
            var controller = new TestOperationController();

            // Act
            Domain.Entities.OperModel operModel = new Domain.Entities.OperModel { x = 1, y = 2 };

            var result = controller.AddTestModel(Newtonsoft.Json.JsonConvert.SerializeObject(operModel));

            // Assert
            Assert.IsNotNull(result);
        }
    }
}