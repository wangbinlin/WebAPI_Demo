﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.IDao
{
    public interface IOperModelDao : IDao<Persistence.Entities.OperModel>
    {
        bool SaveOperModel(Persistence.Entities.OperModel entity);

        IList<Persistence.Entities.OperModel> GetOperModels();

        bool UpdateOperModel(Persistence.Entities.OperModel entity);

        bool DeleteOperModels(IList<Persistence.Entities.OperModel> entities);
    }
}