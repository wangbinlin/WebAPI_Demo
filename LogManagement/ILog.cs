﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogManagement
{
    internal interface ILog : log4net.ILog
    {
        void Debug(string FunctionName, string message);

        void Debug(string FunctionName, string message, System.Exception t);

        void Info(string FunctionName, string message);

        void Info(string FunctionName, string message, System.Exception t);

        void Error(string FunctionName, string message);

        void Error(string FunctionName, string message, System.Exception t);

        void Fatal(string FunctionName, string message);

        void Fatal(string FunctionName, string message, System.Exception t);

        void Warn(string FunctionName, string message);

        void Warn(string FunctionName, string message, System.Exception t);
    }
}