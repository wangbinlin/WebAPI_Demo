﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Core;

namespace LogManagement
{
    internal class LogImpl : log4net.Core.LogImpl, ILog
    {
        private const string ExceptionValue = "987654321";

        /// <summary>
        /// The fully qualified name of this declaring type not the type of any subclass.
        /// </summary>
        public readonly static Type ThisDeclaringType = typeof(LogImpl);

        public LogImpl(ILogger logger, string DiskId)
            : base(logger)
        {
        }

        #region Implementation of IMyLog

        public void Debug(string FunctionName, string message)
        {
            Debug(FunctionName, message, null);
        }

        public void Debug(string FunctionName, string message, System.Exception t)
        {
            if (this.IsDebugEnabled)
            {
                if (t != null)
                {
                    message = string.Format("(Information={0} Message={1} Source={2} TargetSite={3} StackTrace={4})", message, t.Message, t.Source, t.TargetSite, t.StackTrace);
                }
                LoggingEvent loggingEvent = new LoggingEvent(ThisDeclaringType, Logger.Repository, Logger.Name, Level.Debug, message, null);
                loggingEvent.Properties["FunctionName"] = FunctionName;
                Logger.Log(loggingEvent);
            }
        }

        public void Info(string FunctionName, string message)
        {
            Info(FunctionName, message, null);
        }

        public void Info(string FunctionName, string message, System.Exception t)
        {
            if (this.IsInfoEnabled)
            {
                if (t != null)
                {
                    message = string.Format("(Information={0} Message={1} Source={2} TargetSite={3} StackTrace={4})", message, t.Message, t.Source, t.TargetSite, t.StackTrace);
                }
                LoggingEvent loggingEvent = new LoggingEvent(ThisDeclaringType, Logger.Repository, Logger.Name, Level.Info, message, null);
                loggingEvent.Properties["FunctionName"] = FunctionName;
                Logger.Log(loggingEvent);
            }
        }

        public void Warn(string FunctionName, string message)
        {
            Warn(FunctionName, message, null);
        }

        public void Warn(string FunctionName, string message, System.Exception t)
        {
            if (this.IsWarnEnabled)
            {
                if (t != null)
                {
                    message = string.Format("(Information={0} Message={1} Source={2} TargetSite={3} StackTrace={4})", message, t.Message, t.Source, t.TargetSite, t.StackTrace);
                }
                LoggingEvent loggingEvent = new LoggingEvent(ThisDeclaringType, Logger.Repository, Logger.Name, Level.Warn, message, null);
                loggingEvent.Properties["FunctionName"] = FunctionName;
                Logger.Log(loggingEvent);
            }
        }

        public void Error(string FunctionName, string message)
        {
            Error(FunctionName, message, null);
        }

        public void Error(string FunctionName, string message, System.Exception t)
        {
            if (this.IsErrorEnabled)
            {
                if (t != null)
                {
                    message = string.Format("(Information={0} Message={1} Source={2} TargetSite={3} StackTrace={4})", message, t.Message, t.Source, t.TargetSite, t.StackTrace);
                }
                LoggingEvent loggingEvent = new LoggingEvent(ThisDeclaringType, Logger.Repository, Logger.Name, Level.Error, message, null);
                loggingEvent.Properties["FunctionName"] = FunctionName;
                Logger.Log(loggingEvent);
            }
        }

        public void Fatal(string FunctionName, string message)
        {
            Fatal(FunctionName, message, null);
        }

        public void Fatal(string FunctionName, string message, System.Exception t)
        {
            if (this.IsFatalEnabled)
            {
                if (t != null)
                {
                    message = string.Format("(Information={0} Message={1} Source={2} TargetSite={3} StackTrace={4})", message, t.Message, t.Source, t.TargetSite, t.StackTrace);
                }
                LoggingEvent loggingEvent = new LoggingEvent(ThisDeclaringType, Logger.Repository, Logger.Name, Level.Fatal, message, t);
                loggingEvent.Properties["FunctionName"] = FunctionName;
                Logger.Log(loggingEvent);
            }
        }

        #endregion Implementation of IMyLog
    }
}