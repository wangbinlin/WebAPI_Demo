﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LogManagement
{
    /// <summary>
    /// 记录类型
    /// </summary>
    public enum Log4netType
    {
        [Description("Error")]
        Error = 0,

        [Description("Trace")]
        Trace,

        [Description("Info")]
        Info,

        [Description("Debug")]
        Debug
    }
}