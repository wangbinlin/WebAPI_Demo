﻿using log4net.Layout;
using log4net.Layout.Pattern;

namespace LogManagement
{
    public class ReflectionLayout : PatternLayout
    {
        public ReflectionLayout()
        {
            this.AddConverter("property", typeof(ReflectionPatternConverter));
        }

        public class ReflectionPatternConverter : PatternLayoutConverter
        {
            protected override void Convert(System.IO.TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
            {
                if (Option != null)
                {
                    WriteObject(writer, loggingEvent.Repository, LookupProperty(Option, loggingEvent));
                }
                else
                {
                    WriteDictionary(writer, loggingEvent.Repository, loggingEvent.GetProperties());
                }
            }

            private object LookupProperty(string property, log4net.Core.LoggingEvent loggingEvent)
            {
                object propertyValue = string.Empty;
                propertyValue = loggingEvent.Properties[property];

                return propertyValue;
            }
        }
    }
}