﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommHelper
{
    public class KeyNumber
    {
        /// <summary>
        /// 创建表主键 GUID
        /// </summary>
        /// <returns></returns>
        public static string CreateTablePrimaryKey_Guid()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }
    }
}