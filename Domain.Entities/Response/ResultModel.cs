﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ResultModel<T>
    {
        /// <summary>
        /// 计划是否成功
        /// </summary>
        public bool IsSucess { get; set; }

        public string Message { get; set; }

        public string CallbackMethod { get; set; }

        public T Data { get; set; }
    }
}