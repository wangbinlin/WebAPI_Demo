﻿namespace FrmTest
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnGetAll = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lbxCommand = new System.Windows.Forms.ListBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.operModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.operModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGetAll
            // 
            this.btnGetAll.Location = new System.Drawing.Point(12, 452);
            this.btnGetAll.Name = "btnGetAll";
            this.btnGetAll.Size = new System.Drawing.Size(175, 62);
            this.btnGetAll.TabIndex = 5;
            this.btnGetAll.Text = "获取所有测试数据";
            this.btnGetAll.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(197, 452);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(175, 62);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "新增一条数据";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // lbxCommand
            // 
            this.lbxCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbxCommand.FormattingEnabled = true;
            this.lbxCommand.ItemHeight = 12;
            this.lbxCommand.Location = new System.Drawing.Point(0, 0);
            this.lbxCommand.Name = "lbxCommand";
            this.lbxCommand.Size = new System.Drawing.Size(769, 424);
            this.lbxCommand.TabIndex = 3;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(589, 452);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(175, 62);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "删除一条数据";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(391, 452);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(175, 62);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "更新一条数据";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // operModelBindingSource
            // 
            this.operModelBindingSource.DataSource = typeof(Domain.Entities.OperModel);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 527);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnGetAll);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbxCommand);
            this.Name = "FrmMain";
            this.Text = "测试窗口";
            ((System.ComponentModel.ISupportInitialize)(this.operModelBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGetAll;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox lbxCommand;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.BindingSource operModelBindingSource;
    }
}

